new gridjs.Grid({
      columns: ["Nama", "Email", "No Telp"],
      data: [
        ["Dede", "dede@gmail.com", "081081081081"],
        ["Ana", "ana@gmail.com", "082082082082"],
        ["Ria", "ria@gmail.com", "089089089089"],
        ["Tery", "tery@gmail.com", "087087087087"],
        ["Aulia", "aulia@gmail.com", "085085085085"],
        ["Maulana", "maulana@gmail.com", "089708970897"]
      ],
      pagination: {
            limit: 3,
            summary: false
      },
      sort: true,
      search: {
            enable: true
      },

}).render(document.getElementById("wrapper"));

export default Grid;